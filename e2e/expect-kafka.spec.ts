import { Kafka } from "kafkajs";
import { nanoid } from 'nanoid';
import waitForExpect from "wait-for-expect";
import { ExpectKafka, JsonSerializer, BufferSerializer, StringSerializer, MessageSerializer } from '../src';

jest.setTimeout(120000)
waitForExpect.defaults.timeout = 10000;
waitForExpect.defaults.interval = 500;

describe('ExpectKafka', () => {
  const expectKafka = new ExpectKafka(
    new Kafka({
      brokers: [ 'localhost:9092' ]
    })
  );

  beforeAll(async () => {
    await expectKafka.ensureTopics([
      'to-contain-equal',
      'to-contain-object-matching',
      'string-serializer',
      'buffer-serializer',
      'custom-serializer',
    ], {
      numPartitions: 1,
      replicationFactor: 1,
    });
  })

  afterAll(async () => {
    await expectKafka.disconnect();
  });

  it('to-contain-equal', async () => {
    await expectKafka.subscribe('to-contain-equal', new JsonSerializer());

    const value = nanoid();
    await expectKafka.publish('to-contain-equal', JSON.stringify({
      key: value
    }));

    await waitForExpect(() => {
      expect(expectKafka.received('to-contain-equal')).toContainEqual({
        key: value
      });
    });
  });

  it('to-contain-object-matching', async () => {
    await expectKafka.subscribe('to-contain-object-matching', new JsonSerializer());

    const value = nanoid();
    await expectKafka.publish('to-contain-object-matching', JSON.stringify({
      key: 'value'
    }));
    await expectKafka.publish('to-contain-object-matching', JSON.stringify({
      key: value
    }));

    await waitForExpect(() => {
      expect(expectKafka.received('to-contain-object-matching')).toContainObjectMatching({
        key: value
      })
    })
  });

  it('string serializer', async () => {
    await expectKafka.subscribe('string-serializer', new StringSerializer());

    const value = nanoid();
    await expectKafka.publish('string-serializer', value);

    await waitForExpect(() => {
      expect(expectKafka.received('string-serializer')).toContainEqual(value);
    })
  });

  it('buffer serializer', async () => {
    await expectKafka.subscribe('buffer-serializer', new BufferSerializer());

    const value = nanoid();
    await expectKafka.publish('buffer-serializer', value);

    await waitForExpect(() => {
      expect(expectKafka.received('buffer-serializer')).toContainEqual(Buffer.from(value));
    })
  });

  it('custom serializer', async () => {
    const customSerializer: MessageSerializer<number> = {
      serialize: (buffer: Buffer) => parseInt(buffer.toString()),
    };

    await expectKafka.subscribe('custom-serializer', customSerializer);
    await expectKafka.publish('custom-serializer', '1');

    await waitForExpect(() => {
      expect(expectKafka.received('custom-serializer')).toContainEqual(1);
    })
  });
})

FROM node:alpine

WORKDIR /app

RUN apk update && apk add python3 make openssl-dev g++ bash

COPY package*.json ./
RUN npm i

COPY . .

CMD ["npm", "run", "test"]
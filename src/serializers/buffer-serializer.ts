import { MessageSerializer } from "./serializer.interface";

export class BufferSerializer implements MessageSerializer<Buffer> {
  serialize(buffer: Buffer): Buffer {
    return buffer;
  }
}

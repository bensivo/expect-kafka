import { MessageSerializer } from "./serializer.interface";

export class StringSerializer implements MessageSerializer<string> {
  serialize(buffer: Buffer): string {
    return buffer.toString();
  }
}

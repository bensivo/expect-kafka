import { MessageSerializer } from "./serializer.interface";

export class JsonSerializer implements MessageSerializer<any> {
  serialize(buffer: Buffer): any {
    return JSON.parse(buffer.toString());
  }
}

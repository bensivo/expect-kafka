export interface MessageSerializer<T = any>  {
  serialize: (buffer: Buffer) => T;
}

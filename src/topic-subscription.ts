import { Consumer, Kafka } from "kafkajs";
import { MessageSerializer } from "./serializers/serializer.interface";
import { nanoid } from 'nanoid';

export class TopicSubscription {
  received: any[] = [];
  consumer: Consumer;

  public constructor(
    private kafka: Kafka,
    private topic: string,
    private serializer: MessageSerializer,
  ) { }

  async connect() {
    this.consumer = this.kafka.consumer({
      groupId: 'expect-kafka-' + nanoid(9),
    })
    await this.consumer.subscribe({
      topic: this.topic,
    });
    await this.consumer.connect();
    await this.consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        this.received.push(this.serializer.serialize(message.value));
      }
    });
  }

  async disconnect() {
    await this.consumer.disconnect();
  }

  async clear() {
    this.received = [];
  }
}
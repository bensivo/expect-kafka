import { ITopicConfig, Kafka, Producer, Admin } from "kafkajs";
import { BufferSerializer } from "./serializers/buffer-serializer";
import { MessageSerializer } from './serializers/serializer.interface';
import { TopicSubscription } from "./topic-subscription";

export interface EnsureTopicsOptions {
  numPartitions: number,
  replicationFactor: number,
}

export class ExpectKafka {
  private producer: Producer;
  private admin: Admin;

  private subscriptions: Record<string, TopicSubscription> = {};

  constructor(private kafka: Kafka) { }

  async ensureTopics(topics: string[], options: EnsureTopicsOptions) {
    this.admin = this.kafka.admin();
    await this.admin.connect();

    const existing: string[] = await this.admin.listTopics();
    const toCreate: string[] = [];
    for(const topic of topics) {
      if (!existing.includes(topic)) {
        toCreate.push(topic);
      }
    }

    if (toCreate.length > 0) {
      await this.admin.createTopics({
        waitForLeaders: true,
        topics: toCreate.map(t => ({
          topic: t,
          numPartitions: options?.numPartitions ?? 1,
          replicationFactor: options?.replicationFactor ?? 1,
        }))
      });
    }
  }

  async subscribe(topic: string, serializer: MessageSerializer = new BufferSerializer()) {
    const subscription = new TopicSubscription(
      this.kafka,
      topic,
      serializer,
    );
    await subscription.connect();

    this.subscriptions[topic] = subscription;
    return;
  }

  async disconnect() {
    await Promise.all(Object.values(this.subscriptions).map((sub) =>
      sub.disconnect()
    ));

    await this.producer?.disconnect();
    await this.admin?.disconnect();

    this.producer = undefined;
    this.subscriptions = {};
  }

  received(topic: string) {
    return this.subscriptions[topic].received;
  }

  async publish(topic: string, message: Buffer | string | null) {
    if (!this.producer) {
      this.producer = await this.kafka.producer({});

      await this.producer.connect();
    }

    await this.producer.send({
      topic,
      messages: [
        {
          value: message
        }
      ]
    });
  }
}

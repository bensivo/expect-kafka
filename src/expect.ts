import 'jest';
import * as isMatch from 'lodash.isMatch';

declare global {
  namespace jest {
    interface Matchers<R> {
      toContainObjectMatching(value: any): R;
    }
  }
}

expect.extend({
  toContainObjectMatching: (objects: any[], expected: any ) => {
    let pass = objects.some((obj) => {
      return isMatch(obj, expected);
    });

    return {
      message: () => `Expected one of ${objects} to match ${expected}`,
      pass,
    }
  },
})

